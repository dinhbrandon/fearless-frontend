var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
var alertTrigger = document.getElementById('liveAlertBtn')

function alert(message, type) {
  var wrapper = document.createElement('div')
  wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

  alertPlaceholder.append(wrapper)
}

function createCardHtml(name, description, pictureUrl, startDate, endDate, location) {
  return `
  <div class="col">
    <div class="card shadow p-3 mb-2 bg-body-tertiary rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        <p class="card-text">${startDate} - ${endDate}</p>
      </div>
    </div>
  </div>
  `;
}


window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/'; //url to be fetched
  const columns = document.querySelectorAll('.col-4') //DOM node that selects ALL '.col-4' elements
  const row = document.querySelector('.row');
  const container = document.querySelector('.container');
  let columnIndex = 0; //initiate to keep track of the 3 columns in html by index

  try {
    const response = await fetch(url); //waiting for promise object, assigning content of fetched url to 'response'

    if (!response.ok) {
      throw new Error('Response not ok');
    } else {
      const data = await response.json(); //waiting for promise object, assigning parsed json info from url to 'data'
      // renderCard(data.conferences)
      
      
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`; //assigning URL at location of href to detailURL
        const detailResponse = await fetch(detailUrl); //waiting for promise object, assigning content of fetched url to detailResponse
        if (detailResponse.ok) {
          const details = await detailResponse.json(); //parsing json content of promise object into variable called details (in a way JS can read it)
          const title = details.conference.name; //separating components of the parsed JSON into individual variables
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts).toLocaleDateString();
          const endDate = new Date(details.conference.ends).toLocaleDateString();
          const location = details.conference.location.name;
          
          // renderCard(conference); //unable to call on details.conference in renderCard() because we are indexing 

          //prev. a function
          const html = createCardHtml(title, description, pictureUrl, startDate, endDate, location); //Creating a card per iteration of conference
          
          const column = columns[columnIndex % 3]; // modulus returns remainder; therefore 1 % 3 and 2 % 3 would be 1 & 2 consecutively. 3 goes into 1 0 times and 1 is still left.
          column.innerHTML += html;
          columnIndex = (columnIndex + 1) % 3;// modulus on 1 and 2 would still return 1 or 2, but would reset columnIndex to 0 if third iteration

        }
        }

    }
  } catch (e) {
    alert(`${e}`, 'warning')
    console.error('error', e);
  }

});



          // if (conferenceCounter % 3 === 1 && data.conferences.indexOf(conference) === data.conferences.length - 1) {
          //   console.log("this is four");
          // } else if (conferenceCounter % 3 === 2 && data.conferences.indexOf(conference) === data.conferences.length - 1) {
          //   // row.innerHTML += document.createElement("col")
          //   // row.innerHTML += document.createElement("col")
          // } else if (conferenceCounter % 3 === 0) {
          //   container.innerHTML += `<div class="row">${html}</div>`;
          // } else {
          //   row.innerHTML += html;
          // }