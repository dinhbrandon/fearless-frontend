window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const divTag = document.getElementById('loading-conference-spinner')
    const successDivTag = document.getElementById('success-message')
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      divTag.classList.add("d-none");
      selectTag.classList.remove("d-none");


      //create attendee -----> CONTINUE HERE - READ CONSOLE ERRORS
      const attendeeFormTag = document.getElementById('create-attendee-form');
      attendeeFormTag.addEventListener('submit', async event =>{
        event.preventDefault();
        const attendeeFormData = new FormData(attendeeFormTag);
        const jsonData = JSON.stringify(Object.fromEntries(attendeeFormData));
        const attendeesUrl = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: "post",
            body: jsonData,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch (attendeesUrl, fetchConfig);
        if (response.ok) {
            successDivTag.classList.remove("d-none");
            attendeeFormTag.classList.add("d-none");
            attendeeFormTag.reset();
        }



      })

    }
  
  });