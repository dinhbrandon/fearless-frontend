window.addEventListener('DOMContentLoaded', async () => {
    const locationUrl = 'http://localhost:8000/api/locations/';
    const response = await fetch(locationUrl);

    if (response.ok) {

        const data = await response.json();

        const tagSelector = document.getElementById('location')
        const locations = data.locations

  
        for (let location of locations){
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.name;
            tagSelector.appendChild(option);
            
        }


        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const url = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            console.log(json);
    
            const response = await fetch(url, fetchConfig);
    
        if (response.ok) {
            formTag.reset(); 
            
        }
        });

    }
    
})